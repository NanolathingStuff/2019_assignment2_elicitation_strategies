# Processo e Sviluppo Software - Assignment 2

This is the assignment 2 of "Processo e Sviluppo del Software (Blended)"
course Unimib. 
This is an elicitation document overview about '*JORDAN*': a sneaker seller bot.

> **GROUP COMPOSITION:** 

*  Silva Edoardo (matr. 816560)
*  Cocca Umberto (matr. 807191)

> **PIPELINE STAGE**

Pipeline added to be sure the code compiles, so if someone downloads the source and can't compile it, he is sure it's his problem and not code's problem.

> **INFO**

appunti_assignment2.pdf is a file who helped developers in creating the document but IS NOT part of the project.
We realised there were 2 questions usless for the bot during the deployment of the questionnaire (QuestionE4&Questionu8), removed from results but only commented in tex code.